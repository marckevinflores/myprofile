import posts from '~/data/posts.json';
import custom from '~/data/custom.json';
import testimonial from '~/data/testimonial.json';
import projects from '~/data/projects.json';
export default function (context){
  context.store.commit("posts", posts);
  context.store.commit("projects", projects);
  context.store.commit("customize", custom);
  context.store.commit("testimonial", testimonial);

  if(process.client){
    const path = context.route.fullPath;
    // context.store.dispatch("addVisitor", path);
  }
}

