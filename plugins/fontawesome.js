import Vue from 'vue'
import { library, config } from '@fortawesome/fontawesome-svg-core'
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome'
import { faPhoneAlt, faComment, faEnvelope, faMobile, faMapMarkedAlt,
          faLanguage, faGlobeAsia, faGlobe, faFileAlt, faDesktop, faShareAlt,
        faNetworkWired, faThumbtack, faIdCard, faCodeBranch, faUsersCog, faMicrochip, faFile } from '@fortawesome/free-solid-svg-icons'
import { faFacebook, faTwitter, faInstagram, faLinkedin, faGithub, faAngular, faVuejs, faReact, faPhp } from '@fortawesome/free-brands-svg-icons'

config.autoAddCss = false;
config.productionTip = false;
library.add(faFacebook, faTwitter, faInstagram, faLinkedin, faGithub, faPhoneAlt, faComment, faEnvelope, faMobile, faMapMarkedAlt,
  faLanguage, faGlobeAsia, faGlobe, faFileAlt, faDesktop, faShareAlt,
faNetworkWired, faThumbtack, faIdCard, faCodeBranch, faUsersCog, faMicrochip, faFile, faAngular, faVuejs, faReact, faPhp)
Vue.component('fa', FontAwesomeIcon)
