import Vue from 'vue'
import Nav from '@/components/Nav'

import MainHeading from '@/components/MainHeading'


import About from '@/components/About'

import Footer from '@/components/Footer'

Vue.component('Nav', Nav)
Vue.component('MainHeading', MainHeading)
Vue.component('About', About)
Vue.component('Footer', Footer)
/*   UI   */


import AppButton from '@/components/UI/AppButton'
import AppInput from '@/components/UI/AppInput'
import AppModal from '@/components/UI/AppModal'
import Tabs from '@/components/UI/Tabs';
import Tab from '@/components/UI/Tab';
import Container from '@/components/UI/Container';
Vue.component('AppButton', AppButton)
Vue.component('AppInput', AppInput)
Vue.component('AppModal', AppModal)
Vue.component('Tabs', Tabs)
Vue.component('Tab', Tab)
Vue.component('Container', Container)


import ContactForm from '@/components/ContactForm'
import SocialLink from '@/components/SocialLink'
Vue.component('ContactForm', ContactForm)
Vue.component('SocialLink', SocialLink)



import PostList from '@/components/PostList'
Vue.component('PostList', PostList)

import PostPreview from '@/components/PostPreview'
Vue.component('PostPreview', PostPreview)

import Contact from '@/components/Contact'
Vue.component('Contact', Contact)

import Testimonial from '@/components/Testimonial'
Vue.component('Testimonial', Testimonial)

import CanDo from '@/components/CanDo'
Vue.component('CanDo', CanDo)

import Loader from '@/components/Loader';
Vue.component('Loader', Loader)
// import Logo from "~/assets/logo.svg?inline";
// Vue.component('Logo', Logo)

