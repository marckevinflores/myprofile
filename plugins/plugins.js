import Vue from 'vue';

import VueTyper from 'vue-typer'
Vue.use(VueTyper)

import Vuelidate from 'vuelidate'
Vue.use(Vuelidate)

import VueParticlesBg from "particles-bg-vue";
Vue.use(VueParticlesBg);


import "vue-wysiwyg/dist/vueWysiwyg.css";
import wysiwyg from "vue-wysiwyg";
Vue.use(wysiwyg, {hideModules: {
  'link': true,
  'code': true,
  'image': true,
  'table': true,
  'separator': true
}});

// import VueAwesomeSwiper from 'vue-awesome-swiper'

// Vue.use(VueAwesomeSwiper)
import {Swiper as SwiperClass, Autoplay} from 'swiper/swiper.esm'
import getAwesomeSwiper from 'vue-awesome-swiper/dist/exporter'

SwiperClass.use([Autoplay])
Vue.use(getAwesomeSwiper(SwiperClass))
const {Swiper, SwiperSlide} = getAwesomeSwiper(SwiperClass)

// import style
import 'swiper/swiper-bundle.min.css'
