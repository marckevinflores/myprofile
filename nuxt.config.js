const { join } = require('path')
const pkg = require('./package')
export default {
  ssr: true,
  target: 'static',
  /*
  ** Headers of the page
  */
  head: {
    title: process.env.npm_package_name || '',
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: process.env.npm_package_description || '' }
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' }
    ]
  },
  globalName: 'mkf',
  
  /*
  ** Customize the progress-bar color
  */
  //loading: { color: '#fff', height: '10px' },
  /*
  ** Global CSS
  */
 performance: {
  hints: false
 },
  css: [
    // '@/assets/css/main.css',
    // '@/assets/css/grid.css',
    '@fortawesome/fontawesome-svg-core/styles.css',
    // 'swiper/swiper-bundle.min.css'
  ],
  /*
  ** Plugins to load before mounting the App
  */
  plugins: [
    '@plugins/components.js',
    '@plugins/fontawesome.js',
     { src: '@plugins/plugins.js', ssr: false },
     { src: '@plugins/tracker.js', mode: 'client' },

  ],
  /*
  ** Nuxt.js dev-modules
  */
  buildModules: ['@nuxtjs/tailwindcss'
  ],
  tailwindcss: {
    cssPath: '~/assets/css/tailwind.css',
    configPath: 'tailwind.config.js',
    exposeConfig: false,
    config: {}
  },
  /*
  ** Nuxt.js modules
  */
  modules: [
    '@nuxtjs/axios',
    "@nuxtjs/svg",
    ['@nuxtjs/html-minifier', { log: 'once', logHtml: true }],
    [
      "nuxt-compress",
      {
        gzip: {
          cache: true
        },
        brotli: {
          threshold: 10240
        }
      }
    ]

    // 'nuxt-purgecss',
  ],
  // purgeCSS: {
  //   enabled: false , // or `false` when in dev/debug mode
  //   paths: [
  //     'components/**/*.vue',
  //     'layouts/**/*.vue',
  //     'pages/**/*.vue',
  //     'plugins/**/*.js'
  //   ],
  //   styleExtensions: ['.css'],
  //   whitelist: ['body', 'html', 'nuxt-progress'],
  //   extractors: [
  //     {
  //       extractor: content => content.match(/[A-z0-9-:\\/]+/g) || [],
  //       extensions: ['html', 'vue', 'js']
  //     }
  //   ]
  // },
  // generate: {
  //   routes() {
  //     return axios.get('http://localhost:3001/api/posts').then(res => {
  //       return res.data.map(blog => {
  //         let path;
  //         switch(blog.type){
  //           case 1:
  //           path = 'blogs';
  //           break;
  //           case 2:
  //           path = 'projects';
  //           break;
  //           case 3:
  //           path = 'labs';
  //           break;
  //         }
  //             return {
  //               route: `/${path}/${blog.slug}`,
  //               payload: blog
  //             }

  //       })
  //     })
  //   }
  // },
  /*
  ** Build configuration
  */
  build: {
    postcss: {
      plugins: {
        tailwindcss: join(__dirname, 'tailwind.config.js')
      }
    },
    publicPath: '/mkf/',
    loaders: {
      cssModules: {
        modules: {
          localIdentName: "[hash:base64:3]",
        }
      }
    },
    html: {
      minify: true,
      processConditionalComments: true,
        removeEmptyAttributes: true,
        removeRedundantAttributes: true,
        trimCustomFragments: true,
        useShortDoctype: true,
        preserveLineBreaks: false,
        collapseWhitespace: true
    },
    terser: {
      terserOptions: {
        compress: {
          drop_console: true
        }
      }
    },
    plugins: [],
    /*
    ** You can extend webpack config here
    */
    extend (config, ctx) {
    //   if(!ctx.isDev) {
    //     config.output.publicPath = '/mkf/'
    //  }
    }
  },
  render: {
    // Setting up cache for 'static' directory - a year in milliseconds
    // https://web.dev/uses-long-cache-ttl
    static: {
      maxAge: 60 * 60 * 24 * 365 * 1000,
    },
  },
  env:{
    name: "Marc Kevin Flores",
    apiKey: "AIzaSyCMlwBGgTYhDziD3C2t8ejlyQ4fNZMQh-0"
  },
  axios:{
    baseURL:  process.env.NODE_ENV === 'production' ?
    'https://api.marckevinflores.dev/api/' :
    'http://localhost:3001/api/',
  },
  server: {
    port: 3003, // default: 3000
    // host: '192.168.22.5' // default: localhost
  },
  globals: {
    id: globalName => `__${globalName}`,
    nuxt: globalName => `$${globalName}`,
    context: globalName => `__${globalName.toUpperCase()}__`,
    pluginPrefix: globalName => globalName
  },
  router: {
    linkActiveClass: 'mkf-al',
    linkExactActiveClass: 'mkf-eal',
  }
}
// Jul 23 - Aug 06
// Aug 07 - Aug 22
// Aug 23 - Sep 06
// Oct 23 - Nov 06
// Nov 07 - Nov 22
// Nov 23 - Dec 06
// Dec 07 - Dec 22

