import Vuex from 'vuex'
const visitorUrl = 'http://localhost:3001/api/visitors';
const peekUrl = 'http://localhost:3001/api/peek';
const messageUrl = 'http://localhost:3001/api/message';
export const strict = false;
export const state = () => {
  return {
    validate:null,
    customdata:{},
    posts: [],
    testimonials: [],
    post:{},
    projects: [],
    project: {}
  }
}
export const mutations = {
      posts(state, data){
        state.posts = data
      },
      validate(state, value){
        state.validate = value;
        setTimeout(() => {
           state.validate = null;
        }, 4000);
      },
      customize(state, data){
        state.customdata = data
      },
      testimonial(state, data){
        state.testimonials = data
      },
      projects(state, data){
        state.projects = data
      },
      getPost(state, slug){
        const postIndex =  state.posts.findIndex(
          post => post.slug === slug );
        state.post = state.posts[postIndex]
      },
      getProject(state, slug){
        const projects = [].concat.apply([], state.projects.map(p => p.projects))
        const postIndex =  projects.findIndex(
          post => post.slug === slug );
        state.project = projects[postIndex]
      }
}
export const actions = {
      addVisitor(c, fullPath){
        const path = fullPath.split('/');
        let id = path[2] ? c.state.posts[c.state.posts.findIndex(
            post => post.slug === path[2])].id : 0;

        const data = {id: id, slug: fullPath};
        this.$axios.$post('visitors', data)
      },
      addPeek(c, name){
        this.$axios.$post('peek', {name: name})
      },
      addMessage(c, contact){
        this.$axios.$post('message', contact)
      }
}
export const getters = {
    customData(state){
      return state.customdata;
    }
}
